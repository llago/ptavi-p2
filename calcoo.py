#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


class Clase():

    def __init__(self, operador, operando1, operando2):
        self.operador = operador
        self.operando1 = operando1
        self.operando2 = operando2

    def suma(self):
        return self.operando1 + self.operando2

    def resta(self):
        return self.operando1 - self.operando2

    def operar(self):
        if self.operador == 'suma':
            return self.suma()
        elif self.operador == 'resta':
            return self.resta()


if __name__ == "__main__":

    if len(sys.argv) != 4:
        print(" Usage: ./calculadora.py operando1 operador operando 2")
        sys.exit()

    operador = sys.argv[2]

    try:
        operando1 = float(sys.argv[1])
        operando2 = float(sys.argv[3])

    except ValueError:
        print("Me tienes que pasar entreros o floats")
        sys.exit()

    objeto = Clase(operador, operando1, operando2)
    resultado = objeto.operar()
    print(resultado)
