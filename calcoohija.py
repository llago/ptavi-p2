#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from calcoo import Clase


class calculadora(Clase):

    def multi(self):
        return self.operando1 * self.operando2

    def div(self):
        if self.operando2 == 0:
            return print("Division by zero is not allowed")
        else:
            return self.operando1 / self.operando2

    def operar(self):
        if self.operador == 'multiplica':
            return self.multi()
        elif self.operador == 'divide':
            return self.div()
        elif self.operador == 'suma':
            return self.suma()
        elif self.operador == 'resta':
            return self.resta()


if __name__ == "__main__":

    if len(sys.argv) != 4:
        print(" Usage: ./calculadora.py operando1 operador operando 2")
        sys.exit()

    operador = sys.argv[2]
    try:
        operando1 = float(sys.argv[1])
        operando2 = float(sys.argv[3])

    except ValueError:
        print("Me tienes que pasar entreros o floats")
        sys.exit()

    objeto = calculadora(operador, operando1, operando2)
    resultado = objeto.operar()
    print(resultado)
