#!/usr/bin/python3

import sys
from calcoohija import calculadora


if __name__ == "__main__":

    if len(sys.argv) != 2:
        sys.exit("Usage: calcplus.py <input.txt>")

    with open(sys.argv[1]) as entrada:
        for linea in entrada:
            lista_lineas = linea.rstrip('')
            lista_lineas = linea.rstrip('\n')
            linea = lista_lineas.split(',')

            n = len(linea)
            if linea[n-1] == '':
                while linea[n-1] == '':
                    linea.remove('')
                    n = n - 1

            operador = linea[0]
            operandos = linea[1:]

            n = len(operandos)
            s = 0
            operando1 = float(operandos[0])
            operando2 = float(operandos[1])
            objeto = calculadora(operador, operando1, operando2)
            resultado = objeto.operar()
            n = n-2

            if n >= 1:
                while n > 0:
                    s = s + 1
                    operando2 = float(operandos[1 + s])
                    objeto = calculadora(operador, resultado, operando2)
                    resultado = objeto.operar()
                    n = n - 1

            try:
                operando1 = float(operando1)
                operando2 = float(operando2)

            except ValueError:
                sys.exit("Solo se aceptan int y float")

            print(resultado)

        entrada.close()
